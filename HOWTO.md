# How to create a npm library on GitLab

## Initialize

> generate `package.json`
```bash
npm init
# change the version to `0.0.0`
```

## Update `package.json`

- get the project id: `26615926`
- add this entry to `package.json`
  ```json
  "publishConfig": {
    "@tanooki:registry":"https://gitlab.com/api/v4/projects/26615926/packages/npm/"
  }
  ```
- change the `name` field value to: `"name": "@tanooki/gl.cli.js"`
> - `@tanooki` is the name of my group
> - `26615926` is the id of my project

## Prepare CI

- add a `.npmignore` file with this content: `.gitlab-ci.yml`
- create a `.gitlab-ci.yml` file with at least this content:
```yaml
stages:
  - publish

publish:library:
  image: node:12.0-slim
  stage: publish
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - echo "//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}">.npmrc
    - npm publish
```
- commit (on **master**) *(first time)*

## Release process

0. 🚧 *when all is merged on `master`, create a MR for changing version number in package.json and create release note*
1. change the version number in the `package.json` file
2. create a tag with the same version number
3. that's it 🎉

## How to use the library in another project

### Initialize the project

```bash
echo @tanooki:registry=https://gitlab.com/api/v4/packages/npm/ >> .npmrc
npm init
npm install @tanooki/gl.cli.js
```

### Use it with JavaScript

```javascript
const gitlab = require('@tanooki/gl.cli.js').gitlab
const urlEncodedPath = require('@tanooki/gl.cli.js').urlEncodedPath
```

## Refs

- https://docs.gitlab.com/ee/user/packages/npm_registry/

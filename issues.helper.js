/* [DOC]
## Issues Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/issues.html
[DOC] */

const gitlab = require('./index.js').gitlab
const urlEncodedPath = require('./index').urlEncodedPath
const GitLabModel = require('./index').GitLabModel

class Issue extends GitLabModel {
  constructor(data) {
    super(data)
  }
}
/*

GET /projects/:id/issues
GET /projects/:id/issues?assignee_id=5
GET /projects/:id/issues?author_id=5
GET /projects/:id/issues?confidential=true
GET /projects/:id/issues?iids[]=42&iids[]=43
GET /projects/:id/issues?labels=foo
GET /projects/:id/issues?labels=foo,bar
GET /projects/:id/issues?labels=foo,bar&state=opened
GET /projects/:id/issues?milestone=1.0.0
GET /projects/:id/issues?milestone=1.0.0&state=opened
GET /projects/:id/issues?my_reaction_emoji=star
GET /projects/:id/issues?search=issue+title+or+description
GET /projects/:id/issues?state=closed
GET /projects/:id/issues?state=opened

*/

/* [DOC]
### Get the list of the issues of a project

`getProjectIssues` (function)

**parameters**:
  - `project`: path to the project
  - `perPage`
  - `page`
  - `labels`
  - `token`

**return value**: Array of Issue(s)

Refs: https://docs.gitlab.com/ee/api/issues.html#list-project-issues

#### Examples

> WIP
[DOC] */
async function getProjectIssues({project, perPage, page, labels, token}) {
  let projectId = urlEncodedPath({name: project})

  let filter = _ => {
    let conditions = []
    if(labels!==undefined) conditions.push(`labels=${labels}`)
    if(perPage!==undefined) conditions.push(`perPage=${perPage}`)
    if(page!==undefined) conditions.push(`page=${page}`)

    if(conditions.length==0) {
      return undefined
    }
    if(conditions.length==1) {
      return conditions[0]
    } else {
      return conditions.join("&")
    }
  }

  let path = _ => {
    let queryFilter = filter()
    if(queryFilter!==undefined) {
      return `/projects/${projectId}/issues/?${queryFilter}`
    } else {
      return `/projects/${projectId}/issues`
    }
  }

  let queryPath = path()
  //console.log("🎃", queryPath)

  return gitlab({
    method:"GET",
    path: queryPath,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => data.map(record => new Issue(record)))
    .catch(error => {throw error})
}

/* [DOC]
### Create Issue

`createIssue` (function)

**parameters**:
  - `project`: path to the project
  - `title`
  - `description`
  - `labels`
  - `confidential` (`false` by default)
  - `token`

**return value**: Array of Issues

#### TODO
add joint file, note, ... metadata?

#### Examples

> WIP
[DOC] */

async function createIssue({project, title, description, labels, token, confidential}) {
  let projectId = urlEncodedPath({name: project})

  return gitlab({
    method:"POST",
    path: `/projects/${projectId}/issues`,
    data: {title, description, labels, confidential:(confidential || false)},
    token
  }).then(payload => payload.data)
    .then(data => new Issue(data))
    .catch(error => {throw error})
}

module.exports = {
  Issue: Issue,
  Helper: {
    getProjectIssues: getProjectIssues,
    createIssue: createIssue
  }
}

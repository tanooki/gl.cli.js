# Templates

## New helper

```javascript
/* [DOC] 
## <helper_name(plural)> Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/<relevant_html_page>

[DOC] */

const gitlab = require('./index.js').gitlab
const urlEncodedPath = require('./index').urlEncodedPath
const GitLabModel = require('./index').GitLabModel

/* [DOC] 
### <helper_name(plural)> Helper Type(s)

- `<main_type_used_by_the_helper>` (parent: `GitLabModel`)

[DOC] */
class <Type_Name> extends GitLabModel {
  constructor(data) {
    super(data)
  }  
}
```


## New helper's feature

To add a new feature to an helper, use this template

```javascript
/* [DOC]
### Title (explain the goal of the function)

**function** `name_of_the_function`

GitLab refs: https://docs.gitlab.com/ee/api/<relevant_page_and_anchor>

**parameters**:
  - 
  - 

**return value**: 

#### Example

[DOC] */
async function something({parameters}) {

  return gitlab({
    method:"",
    path: `/url/`,
    data: {some_parameters},
    token
  }).then(payload => payload.data) 
    .then(data => new <Type>(data)) 
    .catch(error => {throw error})
}
```

## And don't forget to export the function at the end of the helper's file

```javascript
module.exports = {
  Helper: {
    something: something,
  }
}
```
/* [DOC]
## Projects Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/projects.html

[DOC] */

const fs = require('fs')

const { exec } = require("child_process")

const gitlab = require('./index.js').gitlab
const urlEncodedPath = require('./index').urlEncodedPath
const GitLabModel = require('./index').GitLabModel
const User = require('./users.helper').User

/* [DOC]
### Projects Helper Type

- `Project` (parent: `GitLabModel`)

[DOC] */
class Project extends GitLabModel {
  constructor(data) {
    super(data)
  }
}

/* [DOC]
### Create Project

**function** `createProject`

GitLab refs: https://docs.gitlab.com/ee/api/projects.html#create-project

**parameters**:
  - `name`
  - `path` *(most of the time path=name)*
  - `nameSpaceId`
  - `initializeWithReadMe`
  - `visibility` Can be `private`, `internal`, or `public`
  - `defaultBranch` (`main` by default)
  - `token` (not mandatory)

**return value**: instance of `Project`

#### Example

```javascript
async function create_project_in_group() {
  let subGroup = await GroupsHelper.getSubGroupDetails({subGroup:"tanooki-workshops/my-demo-group/bob-group"})

  let subGroupId = subGroup.get("id")

  try {
    let project = await ProjectsHelper.createProject({
      name: "demo-002",
      path: "demo-002",
      nameSpaceId: subGroupId,
      initializeWithReadMe: true
    })
  } catch(error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}
```

[DOC] */
async function createProject({name, path, nameSpaceId, initializeWithReadMe, visibility, defaultBranch, token}) {
  let namespace_id = nameSpaceId
  let initialize_with_readme = initializeWithReadMe
  return gitlab({
    method:"POST",
    path: `/projects/`,
    data: {name, path, namespace_id, initialize_with_readme, visibility, default_branch: defaultBranch || 'main' },
    token
  }).then(payload => payload.data)
    .then(data => new Project(data))
    .catch(error => {throw error})
}


/* [DOC]
### Create Project for User

> 🚧 WIP (TODO)

[DOC] */
async function createProjectForUser({name, path, namespace_id, token}) {

}

/* [DOC]
### Add member to Project

**function** `addMemberToProject`

GitLab refs: https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project

**parameters**:
  - `project`
  - `userId`
  - `accessLevel`
  - `token` (not mandatory)

**return value**: instance of `User`

#### Access levels

- `10` => Guest access
- `20` => Reporter access
- `30` => Developer access
- `40` => Maintainer access
- `50` => Owner access # Only valid for groups

#### Example

```javascript
try {
  let projectMember = await ProjectsHelper.addMemberToProject({
    project: "tanooki-workshops/WKS-DEMO-01/inscriptions",
    userId: 8446436,
    accessLevel: 40
  })
  console.log(projectMember)
} catch(error) {
  console.log("😡", error.message)
  console.log(error.response.status, error.response.statusText)
  console.log(error.response.data)
}

```

[DOC] */
async function addMemberToProject({project, userId, accessLevel, token}) {
  let projectId = urlEncodedPath({name: project})
  let user_id = userId
  let access_level = accessLevel

  return gitlab({
    method:"POST",
    path: `/projects/${projectId}/members`,
    data: {user_id, access_level},
    token
  }).then(payload => payload.data)
    .then(data => new User(data))
    .catch(error => {throw error})
}

/* [DOC]
### Schedule the export of a Project

**function** `scheduleProjectExport`

GitLab refs: https://docs.gitlab.com/ee/api/project_import_export.html#schedule-an-export

```
POST /projects/:id/export
```

**parameters**:
  - `project`
  - `token` (not mandatory)

**return value**: json message

#### Example

```javascript
try {
  let schedule = await ProjectsHelper.scheduleProjectExport({project:"tanooki-workshops/workshops/hands-on-demo/amazing-project"})
  console.log(schedule)
  console.log(schedule.get("message")) // should be equal to '202 Accepted'
} catch (error) {
  console.log("😡", error.message)
  console.log(error.response.status, error.response.statusText)
  console.log(error.response.data)
}
```
[DOC] */
async function scheduleProjectExport({project, token}) {
  let projectId = urlEncodedPath({name: project})

  return gitlab({
    method:"POST",
    path: `/projects/${projectId}/export`,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => new GitLabModel(data))
    .catch(error => {throw error})

}

/* [DOC]
### Get the status of the export of a Project

**function** `getProjectExportStatus`

GitLab refs: https://docs.gitlab.com/ee/api/project_import_export.html#export-status

```
GET /projects/:id/export
```

**parameters**:
  - `project`
  - `token` (not mandatory)

**return value**: json message

#### Example

```javascript
try {
  let projectExport = await ProjectsHelper.getProjectExportStatus({project:"tanooki-workshops/workshops/hands-on-demo/amazing-project"})
  console.log(projectExport.get("id"), projectExport.get("name"), projectExport.get("export_status"))
  // projectExport.get("export_status") should be equal to 'finished'
  console.log(projectExport.get("_links").api_url)
  console.log(projectExport.get("_links").web_url)

} catch (error) {
  console.log("😡", error.message)
  console.log(error.response.status, error.response.statusText)
  console.log(error.response.data)
}
```
[DOC] */
async function getProjectExportStatus({project, token}) {
  let projectId = urlEncodedPath({name: project})

  return gitlab({
    method:"GET",
    path: `/projects/${projectId}/export`,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => new GitLabModel(data))
    .catch(error => {throw error})

}

/* [DOC]
### Download the export of a Project

**function** `downloadProjectExport`

GitLab refs: https://docs.gitlab.com/ee/api/project_import_export.html#export-download

```
GET /projects/:id/export/download
```

**parameters**:
  - `project`
  - `file` (path and file name)
  - `token` (not mandatory)

**return value**: true if all is ok

#### Example

```javascript
try {
  let result = await ProjectsHelper.downloadProjectExport({project:"tanooki-workshops/workshops/hands-on-demo/amazing-project", file:__dirname+"/hey.gz"})

  console.log(result) // == true if all is ok

} catch (error) {
  console.log("😡", error.message)
  console.log(error.response.status, error.response.statusText)
  console.log(error.response.data)
}
```
[DOC] */
async function downloadProjectExport({project, file, token}) {
  let projectId = urlEncodedPath({name: project})

  const writer = fs.createWriteStream(file)

  return gitlab({
    method:"GET",
    path: `/projects/${projectId}/export/download`,
    data: null,
    responseType: "stream",
    token
  }).then(response => {
      return response.data
    })
    .then(data => {

      return new Promise((resolve, reject) => {
        data.pipe(writer)
        var error = null

        writer.on('error', err => {
          error = err
          writer.close()
          reject(err)
        })

        writer.on('close', () => {
          if (!error) {
            resolve(true)
          }
        })
      })

    })
    .catch(error => {throw error})
}

/* [DOC]
### Import a Project file

🖐️ this method is a "huge hack" and need curl installed

**function** `importProjectFile`

GitLab refs: https://docs.gitlab.com/ee/api/project_import_export.html#import-a-file

```
POST /projects/import
```

**parameters**:
  - `namespace`
  - `file` (file to upload)
  - `path` (it's only the name of the project)
  - `token` (not mandatory)

**return value**: json text

#### Example

```javascript
try {
  let result = await ProjectsHelper.importProjectFile({
    file: __dirname+"/hey.gz",
    namespace:"tanooki-workshops/WKS-DEMO-00",
    path:"amazing-project-tada"
  })
  console.log(result)

} catch (error) {
  console.log("😡", error.message)
  console.log(error.response.status, error.response.statusText)
  console.log(error.response.data)
}
```
[DOC] */
async function importProjectFile({namespace, file, path, token}) {
  let gitlabToken = token || process.env.GITLAB_TOKEN_ADMIN
  let gitlabUrl = process.env.GITLAB_URL || "https://gitlab.com"
  let cmd =`curl --request POST --header "PRIVATE-TOKEN: ${gitlabToken}" \
  --form "namespace=${namespace}" \
  --form "path=${path}" \
  --form "file=@${file}" \
  "${gitlabUrl}/api/v4/projects/import"
  `

  let promise = new Promise((resolve, reject) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error) {
        reject(error)
      } else {
        resolve(stdout)
      }
    })
  })
  return promise

}

module.exports = {
  Project: Project,
  Helper: {
    createProject: createProject,
    createProjectForUser: createProjectForUser,
    addMemberToProject: addMemberToProject,
    scheduleProjectExport: scheduleProjectExport,
    getProjectExportStatus: getProjectExportStatus,
    downloadProjectExport: downloadProjectExport,
    importProjectFile: importProjectFile
  }
}

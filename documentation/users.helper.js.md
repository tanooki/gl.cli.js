## Users Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/users.html


### Users Helper Types

- `User` (parent: `GitLabModel`)


### Search Users by name

**function** `searchUsersByName`

GitLab refs: https://docs.gitlab.com/ee/api/search.html#scope-users

**🖐️ this uses the search API**

**parameters**:
  - name
  - token (not mandatory)

**return value**: Array of instances of `User`

#### Example

```javascript
let users = await UsersHelper.searchUsersByName({
  name: "theroro"
})
console.log("users", users)
```


### Search a User by name

**function** `searchUserByName`

GitLab refs: https://docs.gitlab.com/ee/api/search.html#scope-users

**🖐️ this uses the search API**

**parameters**:
  - name
  - token (not mandatory)

**return value**: instance of `User` (the first user of the list)

#### Example

```javascript
let user = await UsersHelper.searchUserByName({
  name: "swannou"
})
console.log("user", user)
```


### Search a User by name

**function** `gerUserById`

GitLab refs: https://docs.gitlab.com/ee/api/users.html#single-user

**parameters**:
  - id
  - token (not mandatory)

**return value**: instance of `User`

#### Example

```javascript
let swann = await UsersHelper.gerUserById({id:"8446310"})
let harold = await UsersHelper.gerUserById({id:"8446436"})
console.log(swann.get("name"), swann.get("username"))
console.log(harold.get("name"), harold.get("username"))
```

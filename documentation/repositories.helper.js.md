## Repositories Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/repositories.html


### Repositories Helper Type(s)

- `Repository` (parent: `GitLabModel`)
- `RepositoryItem` (parent: `GitLabModel`)
   - can be a `tree`
   - or a `blob`
- `File` (parent: `GitLabModel`)


### List repository tree

**function** `getRepositoryTree`

GitLab refs: https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree

**parameters**:
  - `project`
  - `perPage` (optional)
  - `page` (optional)
  - `token` (optional)

**return value**: an instance of `Repository` with a property named `items`: `repository.get("items")`

#### Example

```javascript
let repository = await RepositoriesHelper.getRepositoryTree({
  project: "tanooki/gl.cli.js"
})
console.log(repository.get("items"))
```


### List repository tree: only items

**function** `getRepositoryTreeItems`

GitLab refs: https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree

**parameters**:
  - `project`
  - `perPage` (optional)
  - `page` (optional)
  - `token` (optional)

**return value**: an array of json objects (a list of repository files and directories in a project)

#### Example

```javascript
let onlyItemsOfTheProject = await RepositoriesHelper.getRepositoryTreeItems({
  project: "tanooki/gl.cli.js"
})
console.log(onlyItemsOfTheProject)
```

### Create file in the repository

**function** `createFile`

GitLab refs: https://docs.gitlab.com/ee/api/repository_files.html#create-new-file-in-repository

**parameters**:
  - `project`
  - `filePath`
  - `branch`
  - `authorEmail`
  - `authorName`
  - `content`
  - `commitMessage`
  - `token`

**return value**: an instance of `File`

#### Example

```javascript
let markdownFile = await RepositoriesHelper.createFile({
  project: "tanooki-workshops/my-demo-group/bob-group/demo-000",
  filePath: "README.md",
  branch: "master",
  authorEmail: "bob@morane.com",
  content: dedent`
    # Awesome project
    This is a WIP 🚧
  `,
  commitMessage: "📝 add the README file"
})
```


### Update file in the repository

**function** `updateFile`

GitLab refs: https://docs.gitlab.com/ee/api/repository_files.html#update-existing-file-in-repository

**parameters**:
  - `project`
  - `filePath`
  - `branch`
  - `authorEmail`
  - `authorName`
  - `content`
  - `commitMessage`
  - `token`

**return value**: an instance of `File`

#### Example

```javascript
let markdownFile = await RepositoriesHelper.updateFile({
  project: "tanooki-workshops/my-demo-group/bob-group/demo-000",
  filePath: "README.md",
  branch: "master",
  authorEmail: "bob@morane.com",
  content: dedent`
    # Awesome project
    This is a WIP 🚧

    ## new title
    > 👋 Hello World 🌍
  `,
  commitMessage: "📝 update the README file"
})
```


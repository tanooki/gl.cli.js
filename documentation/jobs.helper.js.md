## Jobs Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/jobs.html

### Get the list of the jobs of a project

**function** `getProjectJobs`

GitLab refs: https://docs.gitlab.com/ee/api/jobs.html#list-project-jobs

**parameters**:
  - project
  - perPage
  - page
  - token

**return value**: Array of `Job`
> 🖐️ this returns an empty array if no pipeline were run

#### Example

> what
```javascript
let jobsList = await JobsHelper.getProjectJobs({project: "tanooki-workshops/WKS-DEMO-100/codelab"})
```


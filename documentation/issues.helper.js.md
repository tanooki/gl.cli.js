## Issues Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/issues.html

### Get the list of the issues of a project

`getProjectIssues` (function)

**parameters**:
  - `project`: path to the project
  - `perPage`
  - `page`
  - `labels`
  - `token`

**return value**: Array of Issue(s)

Refs: https://docs.gitlab.com/ee/api/issues.html#list-project-issues

#### Examples

> WIP

### Create Issue

`createIssue` (function)

**parameters**:
  - `project`: path to the project
  - `title`
  - `description`
  - `labels`
  - `confidential` (`false` by default)
  - `token`

**return value**: Array of Issues

#### TODO
add joint file, note, ... metadata?

#### Examples

> WIP

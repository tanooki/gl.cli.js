/* [DOC]
## Repositories Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/repositories.html

[DOC] */

const gitlab = require('./index.js').gitlab
const urlEncodedPath = require('./index.js').urlEncodedPath
const GitLabModel = require('./index.js').GitLabModel

/* [DOC]
### Repositories Helper Type(s)

- `Repository` (parent: `GitLabModel`)
- `RepositoryItem` (parent: `GitLabModel`)
   - can be a `tree`
   - or a `blob`
- `File` (parent: `GitLabModel`)

[DOC] */
class Repository extends GitLabModel {
  constructor(data) {
    super(data)
  }
}

class RepositoryItem extends GitLabModel {
  constructor(data) {
    super(data)
  }
}

class File extends GitLabModel {
  constructor(data) {
    super(data)
  }
}

/* [DOC]
### List repository tree

**function** `getRepositoryTree`

GitLab refs: https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree

**parameters**:
  - `project`
  - `perPage` (optional)
  - `page` (optional)
  - `token` (optional)

**return value**: an instance of `Repository` with a property named `items`: `repository.get("items")`

#### Example

```javascript
let repository = await RepositoriesHelper.getRepositoryTree({
  project: "tanooki/gl.cli.js"
})
console.log(repository.get("items"))
```

[DOC] */

async function getRepositoryTree({project, perPage, page, token}) {
  let projectId = urlEncodedPath({name: project})

  let filter = _ => {
    let conditions = []
    if(perPage!==undefined) conditions.push(`perPage =${perPage}`)
    if(page!==undefined) conditions.push(`page=${page}`)

    if(conditions.length==0) {
      return undefined
    }
    if(conditions.length==1) {
      return conditions[0]
    } else {
      return conditions.join("&")
    }
  }

  let path = _ => {
    let queryFilter = filter()
    if(queryFilter!==undefined) {
      return `/projects/${projectId}/repository/tree/?${queryFilter}`
    } else {
      return `/projects/${projectId}/repository/tree`
    }
  }

  let queryPath = path()

  return gitlab({
    method: "GET",
    path: queryPath,
    token: token,
    data: null
  }).then(payload => payload.data)
    .then(data => data.map(record => new RepositoryItem(record)))
    .then(items => new Repository({items:items}))
    .catch(error => {throw error})
}


/* [DOC]
### List repository tree: only items

**function** `getRepositoryTreeItems`

GitLab refs: https://docs.gitlab.com/ee/api/repositories.html#list-repository-tree

**parameters**:
  - `project`
  - `perPage` (optional)
  - `page` (optional)
  - `token` (optional)

**return value**: an array of json objects (a list of repository files and directories in a project)

#### Example

```javascript
let onlyItemsOfTheProject = await RepositoriesHelper.getRepositoryTreeItems({
  project: "tanooki/gl.cli.js"
})
console.log(onlyItemsOfTheProject)
```
[DOC] */

async function getRepositoryTreeItems({project, perPage, page, token}) {
  let projectId = urlEncodedPath({name: project})

  let filter = _ => {
    let conditions = []
    if(perPage!==undefined) conditions.push(`perPage =${perPage}`)
    if(page!==undefined) conditions.push(`page=${page}`)

    if(conditions.length==0) {
      return undefined
    }
    if(conditions.length==1) {
      return conditions[0]
    } else {
      return conditions.join("&")
    }
  }

  let path = _ => {
    let queryFilter = filter()
    if(queryFilter!==undefined) {
      return `/projects/${projectId}/repository/tree/?${queryFilter}`
    } else {
      return `/projects/${projectId}/repository/tree`
    }
  }

  let queryPath = path()

  return gitlab({
    method: "GET",
    path: queryPath,
    token: token,
    data: null
  }).then(payload => payload.data)
    .then(data => data)
    .catch(error => {throw error})
}

/* [DOC]
### Create file in the repository

**function** `createFile`

GitLab refs: https://docs.gitlab.com/ee/api/repository_files.html#create-new-file-in-repository

**parameters**:
  - `project`
  - `filePath`
  - `branch`
  - `authorEmail`
  - `authorName`
  - `content`
  - `commitMessage`
  - `token`

**return value**: an instance of `File`

#### Example

```javascript
let markdownFile = await RepositoriesHelper.createFile({
  project: "tanooki-workshops/my-demo-group/bob-group/demo-000",
  filePath: "README.md",
  branch: "master",
  authorEmail: "bob@morane.com",
  content: dedent`
    # Awesome project
    This is a WIP 🚧
  `,
  commitMessage: "📝 add the README file"
})
```

[DOC] */

async function createFile({project, filePath, branch, authorEmail, authorName, content, commitMessage, token}) {
  let projectId = urlEncodedPath({name: project})
  return gitlab({
    method:"POST",
    path: `/projects/${projectId}/repository/files/${filePath}`,
    token: token, 
    data: {
      branch: branch,
      author_email: authorEmail,
      author_name: authorName, 
      content: content,
      commit_message: commitMessage
    }
  }).then(payload => payload.data)
    .then(data => new File(data))
    .catch(error => {throw error})
}


/* [DOC]
### Update file in the repository

**function** `updateFile`

GitLab refs: https://docs.gitlab.com/ee/api/repository_files.html#update-existing-file-in-repository

**parameters**:
  - `project`
  - `filePath`
  - `branch`
  - `authorEmail`
  - `authorName`
  - `content`
  - `commitMessage`
  - `token`

**return value**: an instance of `File`

#### Example

```javascript
let markdownFile = await RepositoriesHelper.updateFile({
  project: "tanooki-workshops/my-demo-group/bob-group/demo-000",
  filePath: "README.md",
  branch: "master",
  authorEmail: "bob@morane.com",
  content: dedent`
    # Awesome project
    This is a WIP 🚧

    ## new title
    > 👋 Hello World 🌍
  `,
  commitMessage: "📝 update the README file"
})
```

[DOC] */
async function updateFile({project, filePath, branch, authorEmail, authorName, content, commitMessage, token}) {
  let projectId = urlEncodedPath({name: project})
  return gitlab({
    method:"PUT",
    path: `/projects/${projectId}/repository/files/${filePath}`,
    token: token, 
    data: {
      branch: branch,
      author_email: authorEmail,
      author_name: authorName, 
      content: content,
      commit_message: commitMessage
    }
  }).then(payload => payload.data)
    .then(data => new File(data))
    .catch(error => {throw error})
}


module.exports = {
  Repository: Repository,
  Helper: {
    getRepositoryTree: getRepositoryTree,
    getRepositoryTreeItems: getRepositoryTreeItems,
    createFile: createFile,
    updateFile: updateFile
  }
}

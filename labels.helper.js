/* [DOC] 
## Labels Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/labels.html
[DOC] */

const gitlab = require('./index.js').gitlab
const urlEncodedPath = require('./index').urlEncodedPath
const GitLabModel = require('./index').GitLabModel

class Label extends GitLabModel {
  constructor(data) {
    super(data)
  }
}

/* === Create Label (Project) ===
  - https://developer.mozilla.org/en-US/docs/Web/CSS/color_value#color_keywords
*/
async function createLabel({project, name, color, description, priority, token}) {
  let projectId = urlEncodedPath({name: project})
   return gitlab({
    method:"POST",
    path: `/projects/${projectId}/labels`,
    data: {name, color, description, priority},
    token
  }).then(payload => payload.data)
    .then(data => new Label(data))
    .catch(error => {throw error})
}


module.exports = {
  Label: Label,
  Helper: {
    createLabel: createLabel
  }
}

/* [DOC] 
## Jobs Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/jobs.html
[DOC] */

const gitlab = require('./index.js').gitlab
const urlEncodedPath = require('./index').urlEncodedPath
const GitLabModel = require('./index').GitLabModel

class Job extends GitLabModel {
  constructor(data) {
    super(data)
  }
}

/* [DOC]
### Get the list of the jobs of a project

**function** `getProjectJobs`

GitLab refs: https://docs.gitlab.com/ee/api/jobs.html#list-project-jobs

**parameters**:
  - project
  - perPage
  - page
  - token

**return value**: Array of `Job`
> 🖐️ this returns an empty array if no pipeline were run

#### Example

> what
```javascript
let jobsList = await JobsHelper.getProjectJobs({project: "tanooki-workshops/WKS-DEMO-100/codelab"})
```

[DOC] */
async function getProjectJobs({project, perPage, page, token}) {
  let projectId = urlEncodedPath({name: project})

  let filter = _ => {
    let conditions = []

    if(perPage!==undefined) conditions.push(`perPage=${perPage}`)
    if(page!==undefined) conditions.push(`page=${page}`)

    if(conditions.length==0) {
      return undefined
    }
    if(conditions.length==1) {
      return conditions[0]
    } else {
      return conditions.join("&")
    }
  }

  // GET /projects/:id/jobs
  let path = _ => {
    let queryFilter = filter()
    if(queryFilter!==undefined) {
      return `/projects/${projectId}/jobs/?${queryFilter}`
    } else {
      return `/projects/${projectId}/jobs`
    }
  }

  let queryPath = path()

  return gitlab({
    method:"GET",
    path: queryPath,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => data.map(record => new Job(record))) // it's an array
    .catch(error => {throw error})
}

module.exports = {
  Job: Job,
  Helper: {
    getProjectJobs: getProjectJobs
  }
}


const GroupsHelper = require('../groups.helper.js').Helper

async function get_details_of_a_group() {
  let parentGroup = await GroupsHelper.getGroupDetails({group:"tanooki-workshops"})  
  let parent_group_id = parentGroup.get("id")
  console.log(parent_group_id, parentGroup)
}

async function get_details_of_a_sub_group() {
  let parentGroup = await GroupsHelper.getSubGroupDetails({subGroup:"tanooki-workshops/my-demo-group"})

  let parent_group_id = parentGroup.get("id")
  console.log(parent_group_id, parentGroup)
}

async function get_all_sub_groups() {
  try {
    let subgroups = await GroupsHelper.getDescendantGroupsOfGroup({group:"tanooki-workshops"})

    console.log(subgroups.map(group => { return {
      id: group.get("id"),
      name: group.get("name"),
      full_path: group.get("full_path")
    }}))
  } catch (error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function get_all_sub_groups_all_pages() {
  let group = "tanuki-workshops"
  var stop = false, perPage=10, page = 1, groups_list = [], errors = []
  console.log(`🤖 fetching descendant subgroups of ${group}`)

  while(!stop) {
    try {
      let subgroups = await GroupsHelper.getDescendantGroupsOfGroup({group: group, perPage, page})
      if(subgroups.length == 0) { stop = true }
      groups_list = groups_list.concat(subgroups)
    } catch (error) {
      console.log("😡", error.message)
      errors.push({
        message: error.message,
        status: error.response.status,
        statusText: error.response.statusText
      })
    }
    page +=1
  }

  console.log("errors", errors)

  console.log("groups_list", groups_list.map(group => { return {
    id: group.get("id"),
    name: group.get("name"),
    full_path: group.get("full_path")
  }}))

}





//get_details_of_a_group()
//get_details_of_a_sub_group()

//get_all_sub_groups()

get_all_sub_groups_all_pages()






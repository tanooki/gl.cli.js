
const fs = require('fs')

const ProjectsHelper = require('../projects.helper.js').Helper


// https://gitlab.com/tanooki-workshops/workshops/hands-on-demo/amazing-project
async function schedule_project_export() {
  try {
    let schedule = await ProjectsHelper.scheduleProjectExport({project:"tanooki-workshops/workshops/hands-on-demo/amazing-project"})
    console.log(schedule)
    console.log(schedule.get("message")) // should be equal to '202 Accepted' 
  } catch (error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
  
}

async function get_project_export_status() {
  try {
    let projectExport = await ProjectsHelper.getProjectExportStatus({project:"tanooki-workshops/workshops/hands-on-demo/amazing-project"})
    console.log(projectExport.get("id"), projectExport.get("name"), projectExport.get("export_status"))
    // projectExport.get("export_status") should be equal to 'finished' 
    console.log(projectExport.get("_links").api_url)
    console.log(projectExport.get("_links").web_url)

  } catch (error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function download_project_export() {
  
  try {
    let result = await ProjectsHelper.downloadProjectExport({project:"tanooki-workshops/workshops/hands-on-demo/amazing-project", file:__dirname+"/hey.gz"})

    console.log(result) // == true if all is ok

  } catch (error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function import_project_file() {
  try {
    let result = await ProjectsHelper.importProjectFile({
      file: __dirname+"/hey.gz",
      namespace:"tanooki-workshops/WKS-DEMO-00",
      path:"amazing-project-tada"
    })
    console.log(result)
    
  } catch (error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

//schedule_project_export()

//get_project_export_status()

//download_project_export()

import_project_file()

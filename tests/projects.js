
const GroupsHelper = require('../groups.helper.js').Helper
const ProjectsHelper = require('../projects.helper.js').Helper


async function create_project_in_group() {
  let subGroup = await GroupsHelper.getSubGroupDetails({subGroup:"tanooki-workshops/my-demo-group/bob-group"})

  let subGroupId = subGroup.get("id")
  console.log(subGroupId, subGroup)

  try {
    let project = await ProjectsHelper.createProject({
      name: "demo-002",
      path: "demo-002",
      nameSpaceId: subGroupId,
      initializeWithReadMe: true
    })
    console.log(project)
  } catch(error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)

  }
}

create_project_in_group()
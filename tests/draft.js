//const Issue = require('../issues.helper.js').Issue
const IssueHelper = require('../issues.helper.js').Helper

async function query_json_doc() {
  let issues = await IssueHelper.getProjectIssues({project:"tanooki/sandbox", labels:"json-doc"})
  issues.forEach(issue => {
    console.log(issue.get("title"), "->", JSON.parse(issue.get("description")))
    console.log("author:", issue.get("author"))
  })
}

query_json_doc()









//const GroupsHelper = require('../groups.helper.js').Helper
//const ProjectsHelper = require('../projects.helper.js').Helper
const RepositoriesHelper = require('../repositories.helper.js').Helper
const dedent = require('../index.js').dedent

async function some_tests() {

  try {
    let repository = await RepositoriesHelper.getRepositoryTree({
      project: "tanooki/gl.cli.js"
    })
    console.log(repository)
    console.log(repository.get("items"))

    let onlyItemsOfTheProject = await RepositoriesHelper.getRepositoryTreeItems({
      project: "tanooki/gl.cli.js"
    })
    console.log(onlyItemsOfTheProject)

  } catch(error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function create_files() {
  try {
    let markdownFile = await RepositoriesHelper.createFile({
      project: "tanooki-workshops/my-demo-group/bob-group/demo-000",
      filePath: "README.md",
      branch: "master",
      authorEmail: "sam@world.com",
      content: dedent`
        # Awesome project
        This is a WIP 🚧
      `,
      commitMessage: "📝 add the README file"
    })

    console.log(markdownFile)


  } catch(error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

async function update_files() {
  try {
    let markdownFile = await RepositoriesHelper.updateFile({
      project: "tanooki-workshops/my-demo-group/bob-group/demo-000",
      filePath: "README.md",
      branch: "master",
      authorEmail: "sam@world.com",
      content: dedent`
        # Awesome project
        This is a WIP 🚧

        ## new title
        > 👋 Hello World 🌍
      `,
      commitMessage: "📝 update the README file"
    })

    console.log(markdownFile)


  } catch(error) {
    console.log("😡", error.message)
    console.log(error.response.status, error.response.statusText)
    console.log(error.response.data)
  }
}

// some_tests()

// create_files()

update_files()

// tanooki-workshops/my-demo-group/bob-group
//const Issue = require('../issues.helper.js').Issue
const IssueHelper = require('../issues.helper.js').Helper
const ProjectsHelper = require('../projects.helper.js').Helper

async function add_members_to_project() {

  let issues = await IssueHelper.getProjectIssues({project: "tanooki-workshops/WKS-DEMO-01/inscriptions"})

  for (var member in issues) {
    let issue = issues[member]
    console.log(issue.get("author").id, issue.get("author").username, issue.get("title"))

    try {
      let projectMember = await ProjectsHelper.addMemberToProject({
        project: "tanooki-workshops/WKS-DEMO-01/inscriptions",
        userId: issue.get("author").id,
        accessLevel: 40
      })
      console.log(projectMember)
    } catch(error) {
      console.log("😡", error.message)
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
    }

  }

}

async function get_issues_list() {
  let issues = await IssueHelper.getProjectIssues({project: "tanooki-workshops/WKS-DEMO-01/inscriptions"})

  issues.reverse().slice(0, 9).forEach(issue => {
    //console.log("title:", issue.get("title"), "description:", issue.get("description"))
    //console.log(issue.get("author").id, issue.get("author").username)
    console.log(issue.get("iid"), issue.get("id"), issue.get("author").username)
  })
}

//get_issues_list()

//https://gitlab.com/tanooki-workshops/services/hello

async function create_issues() {
  let issue = await IssueHelper.createIssue({
    project: "tanooki-workshops/services/hello",
    title: "Hello this is an issue",
    description: "Read me 😉"
  })
  console.log(issue)
  let confidentialIssue = await IssueHelper.createIssue({
    project: "tanooki-workshops/services/hello",
    title: "Only for your eyes",
    description: "Read me 😉",
    confidential: true
  })
  console.log(issue)

}

create_issues()
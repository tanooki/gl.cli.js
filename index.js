/* [DOC] 
## GitLab API JavaScript Client
> 🚧 this is a work in progress

[DOC] */
const axios = require("axios")

String.prototype.replaceAll = function(search, replacement) {
  let target = this
  return target.split(search).join(replacement)
}

function urlEncodedPath({ name }){
  let encoded = `${name
    .replaceAll("/", "%2F")
    .replaceAll(".", "%2E")
    .replaceAll("-", "%2D")
    .replaceAll("_", "%5F")
    .replaceAll(".", "%2E")}`
  return encoded;
}

function dedent(callSite, ...args) {

  function format(str) {
    let size = -1
    return str.replace(/\n(\s+)/g, (m, m1) => {
      if (size < 0) size = m1.replace(/\t/g, "    ").length
      return "\n" + m1.slice(Math.min(m1.length, size))
    })
  }

  if (typeof callSite === "string") return format(callSite)

  if (typeof callSite === "function") return (...args) => format(callSite(...args))

  let output = callSite
      .slice(0, args.length + 1)
      .map((text, i) => (i === 0 ? "" : args[i - 1]) + text)
      .join("")

  return format(output)
}

function gitlab({method, path, data, token, url, responseType, headers}) {
  let gitlabToken = token || process.env.GITLAB_TOKEN_ADMIN
  let gitlabUrl = url || process.env.GITLAB_URL || "https://gitlab.com"

  // default
  let headersOptions = {
    "Content-Type": "application/json",
    "Private-Token": gitlabToken
  }

  let getHeaders = () => {
    if(headers) {
      
      return Object.assign(headersOptions, headers)
    } else {
      return headersOptions
    }
  }

  let options = {
    method: method,
    url: gitlabUrl + '/api/v4' + path,
    headers: getHeaders(),
    data: data !== null ? JSON.stringify(data) : null
  }

  if(responseType) {
    options.responseType = responseType
  }

  //console.log("🤖", options)

  return axios(options)
}

class GitLabModel {
  constructor(data) {
    this._data = data
  }

  get(field) {
    return this._data[field]
  }

  set(field, value) {
    this._data[field] = value
    return this
  }
}

module.exports = {
  urlEncodedPath: urlEncodedPath,
  gitlab: gitlab,
  dedent: dedent,
  GitLabModel: GitLabModel
}

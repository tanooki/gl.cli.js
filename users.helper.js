/* [DOC] 
## Users Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/users.html

[DOC] */

const gitlab = require('./index.js').gitlab
const urlEncodedPath = require('./index').urlEncodedPath
const GitLabModel = require('./index').GitLabModel

/* [DOC] 
### Users Helper Types

- `User` (parent: `GitLabModel`)

[DOC] */
class User extends GitLabModel {
  constructor(data) {
    super(data)
  }  
}


/* [DOC] 
### Search Users by name

**function** `searchUsersByName`

GitLab refs: https://docs.gitlab.com/ee/api/search.html#scope-users

**🖐️ this uses the search API**

**parameters**:
  - name
  - token (not mandatory)

**return value**: Array of instances of `User`

#### Example

```javascript
let users = await UsersHelper.searchUsersByName({
  name: "theroro"
})
console.log("users", users)
```

[DOC] */
async function searchUsersByName({name, token}) {
  return gitlab({
    method:"GET",
    path: `/search?scope=users&search=${name}`,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => data.map(record => new User(record))) // it's an array
    .catch(error => {throw error})
}

/* [DOC] 
### Search a User by name

**function** `searchUserByName`

GitLab refs: https://docs.gitlab.com/ee/api/search.html#scope-users

**🖐️ this uses the search API**

**parameters**:
  - name
  - token (not mandatory)

**return value**: instance of `User` (the first user of the list)

#### Example

```javascript
let user = await UsersHelper.searchUserByName({
  name: "swannou"
})
console.log("user", user)
```

[DOC] */
async function searchUserByName({name, token}) {
  return gitlab({
    method:"GET",
    path: `/search?scope=users&search=${name}`,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => data.map(record => new User(record))) // it's an array
    .then(data => data[0]) // return the first item of the array
    .catch(error => {throw error})
}

/* [DOC] 
### Search a User by name

**function** `gerUserById`

GitLab refs: https://docs.gitlab.com/ee/api/users.html#single-user

**parameters**:
  - id
  - token (not mandatory)

**return value**: instance of `User`

#### Example

```javascript
let swann = await UsersHelper.gerUserById({id:"8446310"})
let harold = await UsersHelper.gerUserById({id:"8446436"})
console.log(swann.get("name"), swann.get("username"))
console.log(harold.get("name"), harold.get("username"))
```
[DOC] */
async function gerUserById({id, token}) {
  return gitlab({
    method:"GET",
    path: `/users/${id}`,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => new User(data))
    .catch(error => {throw error})
}

module.exports = {
  User: User,
  Helper: {
    searchUsersByName: searchUsersByName,
    searchUserByName: searchUserByName,
    gerUserById: gerUserById
  }
}
/* [DOC]
## Groups Helper
> 🚧 this is a work in progress

GitLab refs: https://docs.gitlab.com/ee/api/groups.html

[DOC] */

const gitlab = require('./index.js').gitlab
const urlEncodedPath = require('./index').urlEncodedPath
const GitLabModel = require('./index').GitLabModel
const User = require('./users.helper').User


/* [DOC]
### Groups Helper Types

- `Group` (parent: `GitLabModel`)
- `SubGroup` (parent: `Group`)

[DOC] */
class Group extends GitLabModel {
  constructor(data) {
    super(data)
  }
}

//class SubGroup extends GitLabModel {
class SubGroup extends Group {
  constructor(data) {
    super(data)
  }
}


/* [DOC]
### Get details of a group

**function** `getGroupDetails`

GitLab Refs: https://docs.gitlab.com/ee/api/groups.html#details-of-a-group

**parameters**:
  - `group`
  - `token`

**return value**: instance of `Group`

#### Example

```javascript
async function get_details_of_a_group() {
  let parentGroup = await GroupsHelper.getGroupDetails({group:"tanooki-workshops"})
  let parent_group_id = parentGroup.get("id")
  console.log(parent_group_id, parentGroup)
}
```

[DOC] */
async function getGroupDetails({group, token}) {
  let groupId = urlEncodedPath({name: group})
  return gitlab({
    method:"GET",
    path: `/groups/${groupId}`,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => new Group(data))
    .catch(error => {throw error})
}

/* [DOC]
### Get details of a sub-group

**function** `getSubGroupDetails`

GitLab Refs: https://docs.gitlab.com/ee/api/groups.html#details-of-a-group

**parameters**:
  - `subGroup`
  - `token`

**return value**: instance of `SubGroup`

#### Example

```javascript
async function get_details_of_a_sub_group() {
  let parentGroup = await GroupsHelper.getSubGroupDetails({subGroup:"tanooki-workshops/my-demo-group"})

  let parent_group_id = parentGroup.get("id")
  console.log(parent_group_id, parentGroup)
}
```

[DOC] */
async function getSubGroupDetails({subGroup, token}) {
  let subGroupId = urlEncodedPath({name: subGroup})
  return gitlab({
    method:"GET",
    path: `/groups/${subGroupId}`,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => new SubGroup(data))
    .catch(error => {throw error})
}

/* [DOC]
### Create a sub-group

**function** `createSubGroup`

GitLab Refs: https://docs.gitlab.com/ee/api/groups.html#details-of-a-group

**parameters**:
  - `parentGroupId`
  - `subGroupPath`
  - `subGroupName`
  - `visibility` Can be `private`, `internal`, or `public`
  - `token`

**return value**: instance of `SubGroup`

#### Example

```javascript
async function create_a_sub_group() {

  let parentGroup = await GroupsHelper.getGroupDetails({group:"tanooki-workshops"})
  let parent_group_id = parentGroup.get("id")

  try {
    let myGroup = await GroupsHelper.createSubGroup({
      parentGroupId: parent_group_id,
      subGroupPath: "my-demo-group",
      subGroupName: "my-demo-group"
    })
    console.log("📝", myGroup)

  } catch(error) {
      console.log("😡", error.message)
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
  }
}

async function create_a_sub_sub_group() {

  let parentGroup = await GroupsHelper.getSubGroupDetails({group:"tanooki-workshops/my-demo-group"})

  let parent_group_id = parentGroup.get("id")

  try {
    let myGroup = await GroupsHelper.createSubGroup({
      parentGroupId: parent_group_id,
      subGroupPath: "sam-group",
      subGroupName: "sam-group"
    })
    console.log("📝", myGroup)
    console.log("🖐️", myGroup.get("web_url"))

  } catch(error) {
      console.log("😡", error.message)
      console.log(error.response.status, error.response.statusText)
      console.log(error.response.data)
  }

}
```
[DOC] */
async function createSubGroup({parentGroupId, subGroupPath, subGroupName, visibility, token}) {

  return gitlab({
    method:"POST",
    path: `/groups`,
    data: {path: subGroupPath, name: subGroupName, parent_id: parentGroupId, visibility},
    token
  }).then(payload => payload.data)
    .then(data => new SubGroup(data))
    .catch(error => {throw error})
}



/* [DOC]
### Add member to Group

**function** `addMemberToGroup`

GitLab refs: https://docs.gitlab.com/ee/api/members.html#add-a-member-to-a-group-or-project

**parameters**:
  - `group`
  - `userId`
  - `accessLevel`
  - `token` (not mandatory)

**return value**: instance of `User`

#### Access levels

- `10` => Guest access
- `20` => Reporter access
- `30` => Developer access
- `40` => Maintainer access
- `50` => Owner access # Only valid for groups

#### Example

```javascript
try {
  let groupMember = await GroupsHelper.addMemberToGroup({
    group: "tanooki-workshops/WKS-DEMO-01",
    userId: 8446436,
    accessLevel: 40
  })
  console.log(groupMember)
} catch(error) {
  console.log("😡", error.message)
  console.log(error.response.status, error.response.statusText)
  console.log(error.response.data)
}

```

[DOC] */
async function addMemberToGroup({group, userId, accessLevel, token}) {
  let groupId = urlEncodedPath({name: group})
  let user_id = userId
  let access_level = accessLevel

  return gitlab({
    method:"POST",
    path: `/groups/${groupId}/members`,
    data: {user_id, access_level},
    token
  }).then(payload => payload.data)
    .then(data => new User(data))
    .catch(error => {throw error})
}

/* [DOC]
### Get the descendant groups of a group

**function** `getDescendantGroupsOfGroup`

GitLab refs: https://docs.gitlab.com/ee/api/groups.html#list-a-groups-descendant-groups

**parameters**:
  - group
  - perPage
  - page
  - token

**return value**: Array of `SubGroup`

#### Example

> get all descendant groups
```javascript
async function get_all_sub_groups() {
  try {
    let subgroups = await GroupsHelper.getDescendantGroupsOfGroup({group:"tanooki-workshops"})

    console.log(subgroups.map(group => { return {
      id: group.get("id"),
      name: group.get("name"),
      full_path: group.get("full_path")
    }}))
  } catch (error) {
    console.log("😡", error.message)
  }
}
```

> get all descendant groups (all pages)
```javascript
async function get_all_sub_groups_all_pages() {
  let group = "tanuki-workshops"
  var stop = false, perPage=10, page = 1, groups_list = [], errors = []
  console.log(`🤖 fetching descendant subgroups of ${group}`)

  while(!stop) {
    try {
      let subgroups = await GroupsHelper.getDescendantGroupsOfGroup({group: group, perPage, page})
      if(subgroups.length == 0) { stop = true }
      groups_list = groups_list.concat(subgroups)
    } catch (error) {
      console.log("😡", error.message)
      errors.push({error)
    }
    page +=1
  }

  console.log("errors", errors)

  console.log("groups_list", groups_list.map(group => { return {
    id: group.get("id"),
    name: group.get("name"),
    full_path: group.get("full_path")
  }}))
}
```

[DOC] */
async function getDescendantGroupsOfGroup({group, perPage, page, token}) {
  let groupId = urlEncodedPath({name: group})

  let filter = _ => {
    let conditions = []

    if(perPage!==undefined) conditions.push(`perPage=${perPage}`)
    if(page!==undefined) conditions.push(`page=${page}`)

    if(conditions.length==0) {
      return undefined
    }
    if(conditions.length==1) {
      return conditions[0]
    } else {
      return conditions.join("&")
    }
  }

  let path = _ => {
    let queryFilter = filter()
    if(queryFilter!==undefined) {
      return `/groups/${groupId}/descendant_groups/?${queryFilter}`
    } else {
      return `/groups/${groupId}/descendant_groups`
    }
  }

  let queryPath = path()

  return gitlab({
    method:"GET",
    path: queryPath,
    data: null,
    token
  }).then(payload => payload.data)
    .then(data => data.map(record => new SubGroup(record))) // it's an array
    .catch(error => {throw error})
}

module.exports = {
  Group: Group,
  SubGroup: SubGroup,
  Helper: {
    createSubGroup: createSubGroup,
    getGroupDetails: getGroupDetails,
    getSubGroupDetails: getSubGroupDetails,
    addMemberToGroup: addMemberToGroup,
    getDescendantGroupsOfGroup: getDescendantGroupsOfGroup
  }
}
